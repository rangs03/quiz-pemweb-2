<?php

use App\Http\Controllers\TasksController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/',[TasksController::class, 'index']);
Route::get('/tasks',[TasksController::class, 'index']);

// create table
// rute untuk mengarah ke form tambah tasks
Route::get('/tasks/create', [TasksController::class, 'create']);
// rute untuk menyimpan data inputan ke db table tasks
Route::post('/tasks', [TasksController::class, 'store']);

// update table
// mengarah ke form edit tasks dengan membawa data berdasarkan id
Route::get('/tasks/{id}/edit', [TasksController::class, 'edit']);
// update kategori berdasarkan id
Route::put('/tasks/{id}', [TasksController::class, 'update']);

// delete table
// rute untuk menghapus data berdasarkan id
Route::delete('/tasks/{id}', [TasksController::class, 'destroy']);

// get data tugas yang sudah selesai
Route::get('/tasks/completed', [TasksController::class, 'CompletedTasks']);

// get data tugas belum selesai
Route::get('/tasks/incomplete', [TasksController::class, 'IncompleteTasks']);

// update status
Route::put('/tasks/{id}/status', [TasksController::class, 'statusUpdate']);

// menampilkan detail data tasks tertentu berdasarkan id
Route::get('/tasks/{id}', [TasksController::class, 'show']);

