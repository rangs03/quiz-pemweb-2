@extends('welcome')

@section('title')
    Detail Tugas
@endsection

@push('scripts')
<script src="https://cdn.ckeditor.com/ckeditor5/38.0.1/classic/ckeditor.js"></script>
<script>
    ClassicEditor
        .create( document.querySelector( '#deskripsi' ) )
        .catch( error => {
            console.error( error );
        } );
</script>
@endpush

@section('content')
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body">
            <a href="/" class="btn btn-primary btn-sm my-2">Kembali</a>
            <h4 class="my-2">{{ $data_tasks->judul }}</h4>
            @if ($data_tasks->status == "Selesai")
            <h6 class="btn btn-success ">{{ $data_tasks->status }}</h6>
            @else
            <h6 class="btn btn-danger ">{{ $data_tasks->status }}</h6>
            @endif
            <p >
                {!! $data_tasks->deskripsi !!}
            </p>
        </div>
    </div>
@endsection