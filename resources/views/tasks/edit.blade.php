@extends('welcome')

@section('title')
    Edit Data Tugas
@endsection

@push('scripts')
<script src="https://cdn.ckeditor.com/ckeditor5/38.0.1/classic/ckeditor.js"></script>
<script>
    ClassicEditor
        .create( document.querySelector( '#deskripsi' ) )
        .catch( error => {
            console.error( error );
        } );
</script>
@endpush

@section('content')
<form method="POST" action="/tasks/{{ $data_tasks->id }}">
    @csrf
    @method('put')
    <div class="form-group">
        <label for="exampleInputJudul">Judul</label>
        <input type="text" name="judul" class="form-control" value="{{ $data_tasks->judul }}">
    </div>
    @error('judul')
    <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
        <label for="exampleInputDeskripsi">Deskripsi</label>
        <textarea name="deskripsi" id="deskripsi" cols="30" rows="20"
            class="form-control">{{ $data_tasks->deskripsi }}</textarea>
    </div>
    @error('deskripsi')
    <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
        <label for="exampleInputStatus">Status</label>
        <select name="status" id="" class="form-control">
            @foreach ($list as $list)
                @if ($list == $data_tasks->status)
                    <option value="{{ $list }}" selected>{{ $list }}</option>
                @else
                    <option value="{{ $list }}">{{ $list }}</option>   
                @endif              
            @endforeach
        </select>
    </div>
    @error('status')
    <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection