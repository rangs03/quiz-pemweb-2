@extends('welcome')

@section('title')
Form Tambah List Tugas
@endsection

@push('styles')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
    $(document).ready(function () {
        $('#formTask').select2();
    });
</script>

<script src="https://cdn.ckeditor.com/ckeditor5/38.0.1/classic/ckeditor.js"></script>
<script>
    ClassicEditor
        .create( document.querySelector( '#deskripsi' ) )
        .catch( error => {
            console.error( error );
        } );
</script>

@endpush

@section('content')
<form method="POST" action="/tasks">
    @csrf
    <div class="form-group">
        <label for="exampleInputJudul">Judul</label>
        <input type="text" name="judul" class="form-control">
    </div>
    <div class="form-group">
        @error('judul')
        <div class="alert alert-danger">{{$message}}</div>
        @enderror
    </div>
    <div class="form-group">
        <label for="exampleInputDeskripsi">Deskripsi</label>
        <textarea name="deskripsi" id="deskripsi" cols="30" rows="50" class="form-control"></textarea>
    </div>
    @error('deskripsi')
    <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
        <label for="exampleInputStatus">Status</label>
        <select name="status" id="" class="form-control">
            <option value="">--Pilih Status--</option>
            <option value="Belum selesai">Belum Selesai</option>
            <option value="Selesai">Selesai</option>
        </select>
    </div>
    @error('status')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection