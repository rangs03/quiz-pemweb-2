@extends('welcome')

@section('title')
    List Tugas
@endsection

@push('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css"/>
@endpush

@push('scripts')
    <script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
    <script>
        $(function () {
            $("#dataTable").DataTable();
        });
    </script>
@endpush

@section('content')
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="d-inline">
                <a href="/tasks/create" class="btn btn-primary btn-sm my-2">Tambah</a>
                <a href="/tasks" class="btn">Semua Tugas</a>
                <a href="/tasks/completed" class="btn">Tugas Selesai</a>
                <a href="/tasks/incomplete" class="btn">Tugas Belum Selesai</a>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Judul</th>
                            <th>Deskripsi</th>
                            <th>Status</th>
                            <th>Aksi</th>
                    </thead>
                    <tbody>
                        @forelse ($filtered as $key => $item)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $item->judul }}</td>
                            <td>{{ Str::limit($item->deskripsi, 50)}}</td>
                            <td>
                                @if ("Selesai" ==  $item->status )
                                    <button type="button" class="btn btn-success btn-sm">{{ $item->status }}</button>
                                @else
                                    <button type="button" class="btn btn-danger btn-sm">{{ $item->status }}</button>
                                @endif
                            </td>
                            <td>
                                <form action="/tasks/{{$item->id}}" method="POST">
                                    @csrf
                                    @method('delete')
                                    <a href="/tasks/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                                    <a href="/tasks/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                                </form>
                            </td>
                        </tr>
                        @empty
                            
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection