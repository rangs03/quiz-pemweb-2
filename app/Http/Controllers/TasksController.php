<?php

namespace App\Http\Controllers;

use App\Models\Tasks;
use Illuminate\Console\View\Components\Task;
use Illuminate\Http\Request;
use Illuminate\View\View;

class TasksController extends Controller
{
    public function index(): View
    {
        // get data task
        $data_tasks = Tasks::all();
        $list = array('Belum selesai', 'Selesai');
        // return view
        return view('tasks.index', compact('data_tasks', 'list'));
    }
    
    public function create() 
    {
        // get data task
        $data_tasks = Tasks::all();
        // return view
        return view('tasks.create', compact('data_tasks'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'deskripsi' => 'required',
            'status' => 'required'
        ]);

        $data_tasks = new Tasks;

        $data_tasks->judul = $request['judul'];
        $data_tasks->deskripsi = $request['deskripsi'];
        $data_tasks->status = $request['status'];

        $data_tasks->save();

        // Alert::success('Berhasil', 'Berhasil Menambah Tugas');

        return redirect('/');
        
    }

    public function show($id)
    {
        $data_tasks = Tasks::find($id);

        return view('tasks.detail',compact('data_tasks'));

    }

    public function edit($id)
    {
        $data_tasks = Tasks::find($id);
        $list = array('Belum selesai', 'Selesai');
        return view('tasks.edit',compact('data_tasks', 'list'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'deskripsi' => 'required',
            'status' => 'required'
        ]);

        $data_tasks = Tasks::find($id);

        $data_tasks->judul = $request['judul'];
        $data_tasks->deskripsi = $request['deskripsi'];
        $data_tasks->status = $request['status'];

        $data_tasks->save();
        
        // Alert::success('Berhasil', 'Berhasil Mengedit Peran');

        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data_task = Tasks::find($id);

        $data_task->delete();

        // Alert::success('Berhasil', 'Berhasil Menghapus Tasks');

        return redirect('/');
    }

    public function CompletedTasks()
    {
        $data_tasks = Tasks::where('status', 'Selesai');

        $filtered = $data_tasks->get();

        return view('tasks.filtered', compact('filtered'));
    }

    public function IncompleteTasks()
    {
        $data_tasks = Tasks::where('status','=', 'Belum selesai');

        $filtered = $data_tasks->get();

        return view('tasks.filtered', compact('filtered'));
    }

    public function statusUpdate($id, Request $status)
    {   
        $data_tasks = Tasks::find($id);

        $data_tasks->update([
            'status' => $data_tasks->status === 'Selesai' ? 'Belum selesai' : 'Selesai',
        ]);

        return redirect('/');
    }
}
